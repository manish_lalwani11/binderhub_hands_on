from typing import Any, NamedTuple, Tuple, Union, cast
from ska_tmc_cdm.messages.central_node.cspimport CSPConfiguration
from .base import encoded


class CSPConfig():
    def _generate_csp_assign_resources_config(self):
        interface = "https://schema.skao.int/ska-low-csp-assignresources/2.0"
        common ={
                    "subarray_id": 1
        }
        lowcbf = {
      "resources": [
        {
          "device": "fsp_01",
          "shared": True,
          "fw_image": "pst",
          "fw_mode": "unused"
        },
        {
          "device": "p4_01",
          "shared": True,
          "fw_image": "p4.bin",
          "fw_mode": "p4"
        }
      ]
    }
        return CSPConfiguration(
            interface  = interface,
            common = common,
            lowcbf = lowcbf
        )

    @encoded
    def generate_csp_assign_resources_config(self):
        return self._generate_csp_assign_resources_config()

