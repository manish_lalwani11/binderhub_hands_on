"""
Main entry point class for Jupyter notebook
"""
import json
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from .base import encoded
from .mccs import MCCSConfig
from .sdp_config import SdpConfig
from .csp_config import CSPConfig

class Observation(SdpConfig,MCCSConfig,CSPConfig):
    def _generate_assign_resources_config(self, subarray_id: int = 1):
        interface = "https://schema.skao.int/ska-low-tmc-assignresources/3.0"
        transaction_id = "txn-....-00001"
        subarray_id =  1
        assign_request = AssignResourcesRequest(
            interface = interface,
            transaction_id = transaction_id,
            subarray_id=subarray_id,
            mccs= self.generate_mccs_assign_resources_config().as_object,
            sdp_config=self.generate_sdp_assign_resources_config().as_object,
            csp_config = self.generate_csp_assign_resources_config().as_object
        )
        return assign_request

    @encoded
    def generate_assign_resources_config(self, subarray_id: int = 1):
        return self._generate_assign_resources_config(subarray_id)


    def generate_release_all_resources_config_for_central_node(self, subarray_id: int = 1) -> str:
        config = {
            "interface": "https://schema.skao.int/ska-tmc-releaseresources/2.1",
            "transaction_id": "txn-....-00001",
            "subarray_id": subarray_id,
            "release_all": "true",
            "receptor_ids": [],
        }

        return json.dumps(config)

