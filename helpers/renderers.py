from collections import OrderedDict
from typing import Literal, NamedTuple, TypedDict, cast

from bokeh.io import push_notebook, show
from bokeh.models import ColumnDataSource, LabelSet
from bokeh.plotting import figure

from .base import (
    SubarrayConfigurationState,
    SubarrayResourceState,
    SubarrayScanningState,
)

ItemStates = Literal["DISABLED", "BUSY", "ACTIVE", "OFFLINE"]

state_mapping_to_clr: dict[ItemStates, str] = {
    "DISABLED": "darkmagenta",
    "BUSY": "yellow",
    "ACTIVE": "forestgreen",
    "OFFLINE": "pink",
}

BoxLabels = Literal["On/Off", "Resources Assigned?", "Subarray Configured?"]


class LabeledBlock(NamedTuple):
    text: BoxLabels
    colour: ItemStates


class LabeledBoxesData(TypedDict):
    x_position: list[float]
    y_position: list[float]
    square_labels: list[BoxLabels]
    square_colours: list[str]


class MonitorPlot:
    def __init__(self, plot_width: int, plot_height: int) -> None:
        self._monitor_plot = figure(
            plot_width=plot_width,
            plot_height=plot_height,
            toolbar_location=None,
            x_range=(-0.5, 1.5),
            y_range=(-0.5, 1.5),
        )
        self._monitor_plot.axis.visible = False
        self._monitor_plot.grid.visible = False
        self._labeled_blocks = OrderedDict[BoxLabels, ItemStates](
            {
                "On/Off": "DISABLED",
                "Resources Assigned?": "DISABLED",
                "Subarray Configured?": "DISABLED",
            }
        )
        self._monitor_source = ColumnDataSource(
            LabeledBoxesData(
                x_position=[0.5, 0.5, 0.5],
                y_position=[0.0, 0.5, 1.0],
                square_labels=list(self._labeled_blocks.keys()),
                square_colours=self._colours_as_list,
            )
        )

        self._labels = LabelSet(
            x="x_position",
            y="y_position",
            text="square_labels",
            x_offset=-200,
            y_offset=-10,
            render_mode="canvas",
            text_color="black",
            source=self._monitor_source,
        )
        self._handle = None
        self._indexes = {"on/off": 0, "resources_assigned": 1, "other thing": 2}
        self.on_off_state: Literal["ON", "OFF", "OFFLINE"] = "OFF"
        self.resourcing_state: SubarrayResourceState = "EMPTY"
        self.configuration_state: SubarrayConfigurationState = "NOT_CONFIGURED"
        self.scanning_state: SubarrayScanningState = "NOT_SCANNING"

    def _create_output(self):
        self._monitor_plot.square(
            x="x_position",
            y="y_position",
            size=20,
            color="square_colours",
            source=self._monitor_source,
        )
        self._monitor_plot.add_layout(self._labels)

    def show(self):
        self._create_output()
        self._handle = show(self._monitor_plot, notebook_handle=True)

    def re_render(self):
        self._create_output()
        push_notebook(handle=self._handle)

    @property
    def _colours_as_list(self) -> list[str]:
        return list(
            state_mapping_to_clr.get(value, "DISABLED") for value in self._labeled_blocks.values()
        )

    def _update_data_source(self):
        cast(LabeledBoxesData, self._monitor_source.data)["square_colours"] = self._colours_as_list

    def _set_box(self, box_name: BoxLabels, value: ItemStates):
        self._labeled_blocks[box_name] = value
        self._update_data_source()
        self.re_render()

    def set_resources_assigning(self):
        self._set_box("Resources Assigned?", "BUSY")
        self.resourcing_state = "RESOURCING"

    def set_resources_assigned(self):
        self._set_box("Resources Assigned?", "ACTIVE")
        self.resourcing_state = "COMPOSED"

    def set_resources_removed(self):
        self._set_box("Resources Assigned?", "DISABLED")
        self.resourcing_state = "EMPTY"

    def set_configuring(self):
        self._set_box("Subarray Configured?", "BUSY")
        self.configuration_state = "CONFIGURING"

    def set_configured(self):
        self._set_box("Subarray Configured?", "ACTIVE")
        self.configuration_state = "READY"
        if self.scanning_state == "SCANNING":
            self.scanning_state = "NOT_SCANNING"

    def set_configuration_cleared(self):
        self._set_box("Subarray Configured?", "DISABLED")
        self.configuration_state = "NOT_CONFIGURED"

    def set_scanning(self):
        self.configuration_state = "READY"
        self.scanning_state = "SCANNING"

    def set_not_scanning(self):
        self.configuration_state = "READY"
        self.scanning_state = "NOT_SCANNING"

    def set_on(self):
        self._set_box("On/Off", "ACTIVE")
        self.on_off_state = "ON"

    def set_off(self):
        self._set_box("On/Off", "DISABLED")
        self.on_off_state = "OFF"

    def set_offline(self):
        self._set_box("On/Off", "OFFLINE")
        self.on_off_state = "OFFLINE"

    def observe_telescope_on_off(self, state: Literal["ON", "ERROR", "OFFLINE", "OFF"]):
        if state == "ON":
            self.set_on()
        elif state == "OFFLINE":
            self.set_offline()
        else:
            self.set_off()

    def observe_subarray_resources_state(self, state: SubarrayResourceState):
        if state != self.resourcing_state:
            if state == "COMPOSED":
                self.set_resources_assigned()
            elif state == "RESOURCING":
                self.set_resources_assigning()
            else:
                self.set_resources_removed()

    def observe_subarray_configuration_state(self, state: SubarrayConfigurationState):
        if state != self.resourcing_state:
            if state == "READY":
                self.set_resources_assigned()
                self.set_configured()
            elif state == "CONFIGURING":
                self.set_configuring()
            else:
                self.set_configuration_cleared()

    def observe_subarray_scanning_state(self, state: SubarrayScanningState):
        if state == "SCANNING":
            self.set_scanning()
        elif state == "NOT_SCANNING":
            self.set_not_scanning()
